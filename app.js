const api = require("./config/api.js");
const util = require("./utils/util.js");
App({
  onLaunch (options) {
    const updateManager = wx.getUpdateManager();
    wx.getUpdateManager().onUpdateReady(function () {
      wx.showModal({
        title: "更新提示",
        content: "新版本已经准备好，是否重启应用？",
        success: function (res) {         
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate();           
          }
        }
      });
    });
  },
  ConfigData:undefined,
  DictData:undefined,
  BaseUrl:api.BaseUrl,
  getArticleList:function(typeId,isTop,channel,limit){
    isTop=isTop||0;
    return new Promise(function (resolve, reject){
      util.request(api.ArticleList, 
        { typeId: typeId ,
        isTop:isTop,
        channel:channel||"" 
      }).then(res=>{
        if (res.code == 200) {
          const result = res.data    
          resolve(res.data);
        } else{
          reject(res.msg);
        }
      })  
    })
  },
  loginIf() {
    // if(!util.getStorageSync("token")){
    //   wx.navigateTo({
    //     url: "/pages/auth/login/login",
    //   })
    // }
    // wx.checkSession({
    //   success () {},
    //   fail () {
    //     wx.navigateTo({
    //       url: "/pages/auth/login/login",
    //     })
    //   }
    // })
    this.isLogin((login)=>{
      if(!login)
      wx.navigateTo({
        url: "/pages/auth/login/login",
      })
    })
  },
  isLogin(callbak){
    if(!wx.getStorageSync("token")){
      callbak(false)
      return;
    }
    wx.checkSession({
      success () {
        callbak(true)
      },
      fail () {
        callbak(false)
      }
    })
  },
  getConfigInfo(key,callbak){
    util.request(api.Config,{"configName":"小程序"}).then(res=>{
      var config=res.data.find(item=>item.configKey==key);
      callbak(config)
    });
  }
})
