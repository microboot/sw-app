var WxParse = require('../../lib/wxParse/wxParse.js')
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    html: {
      type: String,
      value: '',
      observer:function(newval, oldval){
        this.renderToHtml(newval);
      }
    },
    text:{
      type: String,
      value: '2222'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },
  lifetimes: {
    attached:function(){
      
    },
    ready: function() {
    
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  pageLifetimes:{
    show: function() {
     
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    renderToHtml(html){
        WxParse.wxParse('content', 'html', html, this)
    }
  }
})