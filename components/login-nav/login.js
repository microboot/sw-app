const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    isLogin:false
  },
  pageLifetimes:{
    attached:function(){
     
    },
    show(){
      this.load();
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    load(){
      var that=this;
      app.isLogin(status=>{
        that.setData({isLogin:status})
        if(status){
          var info=user.getLoginInfo();
          that.setData({
            userInfo:JSON.parse(info)
        })
        }else{
          that.setData({ userInfo:this.data.defaultUserInfo })        
        }
      })
    },
    login(){
      wx.navigateTo({
        url: '/pages/auth/login/login',
      })
    }
  }
})