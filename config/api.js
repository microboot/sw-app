// 以下是业务服务器API地址
// 本机开发时使用
// var WxApiRoot = "http://10.88.213.103:9001/wx/";
// 云平台部署时使用（uat环境）
//var WxApiRoot = "http://39.106.213.73:30303/wx/";
// 云平台上线时使用（prd环境）
var WxApiRoot = "https://swj.goldnewz.cn/front/";
var WxApiAdminRoot = "https://sjs.goldnewz.cn/admin/";
var LocalWxApiRoot ="http://localhost:8080/";

module.exports = {
  BaseUrl:WxApiRoot,
  LoginAndBindPhone:WxApiRoot+"wx/auth/loginAndBindPhone",
  updateUser:WxApiRoot+"wx/auth/update",
  DictData:WxApiRoot+ "wx/dict/all", //字典数据
  ArticleList:WxApiRoot + "wx/article/list", //活动文章接口
  ArticleInfo:WxApiRoot + "wx/article", //活动文章接口
  UploadFile:WxApiRoot + "common/upload", //上传
  Config:WxApiRoot + "wx/config/list", //配置
  ArticleRecordInfo:WxApiRoot+"wx/article_record/getByArticle", //活动记录详情
  ArticleRecordAdd:WxApiRoot+"wx/article_record/add", //添加活动记录
  getMyJoin:WxApiRoot+"wx/article_record/getMyJoin", //我参与的活动
  CardActivityList:WxApiRoot+"wx/card_activity/getList", //打卡地图
  CardActivityLineList:WxApiRoot+"wx/card_activity/getLine", //打卡地图路线
  CardActivityRecoredSubmit:WxApiRoot+"wx/card_activity/submitCard", //打卡
  GetMyPrize:WxApiRoot+"wx/card_activity/getMyPrise", //我的奖励
  GetCoupons:WxApiRoot+"wx/coupon/getCoupons", //优惠券列表
  GetMyCoupons:WxApiRoot+"wx/coupon/getMyCoupons", //我的优惠券列表
  ObtainCoupon:WxApiRoot+"wx/coupon/obtainCoupon", //获得优惠券
  ConfirmCoupon:WxApiRoot+"wx/coupon/confirmCoupon", //核销优惠券
};
