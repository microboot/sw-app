const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isHidden: true,// 默认隐藏
    info: {},
    isJoin: false,
    applyName: "",
    applyPhone: "",
    content: "123",
    isLogin:"",
    activityId:0,
  },
  toggleHidden: function () {
    this.setData({
      isHidden: !this.data.isHidden
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({activityId:options.id})
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    util.request(`${api.ArticleInfo}/${this.data.activityId}`).then(res =>{
      this.setData({ info: res.data })
      this.getArticleRecord();
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getArticleRecord() {
    util.request(api.ArticleRecordInfo, { id: this.data.info.id }).then(res => {
      this.setData({ recordInfo: res.data })
      this.setData({ 
        isJoin:res.data.id,
        applyName: res.data.applyName,
        applyPhone: res.data.applyPhone,
        content: res.data.content,
       })
    })
  },
  submit() {
    util.post(api.ArticleRecordAdd, {
      articleId:this.data.info.id,
      applyName: this.data.applyName,
      applyPhone: this.data.applyPhone,
      content: this.data.content,
    }).then(res => {
      this.setData({ isHidden: true })
      wx.showToast({
        title: '提交成功',
      })
    })
  }
})