var api = require('../../../config/api.js');
var util = require('../../../utils/util.js');
var user = require('../../../utils/user.js');

var app = getApp();
Page({
  data: {
    username: '',
    company: '',
    jobTitle: '',
    show1: false, //弹窗
    show2: false, //弹窗
    array: [],
    typeIndex: 0,
    allow:false,
  },

  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成
    this.getEnterpriseType();
  },
  onReady: function () {

  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },
  accountLogin: function () {
    
    if(!this.data.allow){
      wx.showModal({
        title: '错误信息',
        content: '请同意服务协议',
        showCancel: false
      });
      return;
    }
    var that = this;
    if (that.data.username.length < 1 || that.data.company.length < 1 || that.data.jobTitle.length < 1) {
      wx.showModal({
        title: '错误信息',
        content: '请输入完整信息',
        showCancel: false
      });
      return false;
    }
  var typeItem=this.data.array[this.data.typeIndex];
  app.addScore(util.scoreType.info);
    util
      .request(
        api.AuthLoginByAccount, {
        username: that.data.username,
        company: that.data.company,
        jobTitle: that.data.jobTitle,
        enterpriseTypeId:typeItem.id,
        enterpriseTypeName:typeItem.name
      },
        "POST"
      )
      .then(function (res) {
        if (res.errno === 0) {
          app.globalData.isFill = true;
          util.setStorageSync("hasLogin",true,48*60);   
          wx.showToast({
            title: "成功",
            icon: "success",
            duration: 2000,
          })
          setTimeout(() => {
            wx.switchTab({
              url: '/pages/index/index'
            })
          }, 1500)
        } else {
          app.globalData.isFill = false;
        }
      });
  },
  bindUsernameInput: function (e) {

    this.setData({
      username: e.detail.value
    });
  },
  bindCompanyInput: function (e) {

    this.setData({
      company: e.detail.value
    });
  },
  bindJobInput: function (e) {

    this.setData({
      jobTitle: e.detail.value
    });
  },
  bindPickerChange(e) {
    this.setData({
      typeIndex: e.detail.value
    })
  },
  clearInput: function (e) {
    console.log(11111, e.currentTarget)
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-company':
        this.setData({
          company: ''
        });
        break;
      case 'clear-job':
        this.setData({
          jobTitle: ''
        });
        break;
    }
  },

  onClose: function () {
    this.setData({
      show1: false,
      show2: false,
    });
  },

  handle1: function () {
    this.setData({
      show1: true
    })
  },
  handle2: function () {
    this.setData({
      show2: true
    })
  },
  // 获取用户信息
  getEnterpriseType() {
    util.request(api.enterpriseType).then((res) => {
      if (res.errno === 0) {
        this.setData({
          array: res.data
        })
        this.getUserInfo();        
      }
    })
  },
  // 获取用户信息
  getUserInfo() {
    util.request(api.UserIndex).then((res) => {
      if (res.errno === 0) {   
        var typeIndex=0; 
        this.data.array.forEach((item,index)=>{
          if(item.id==res.data.enterpriseTypeId){
            typeIndex=index;
          }
        })   
        this.setData({
          username: res.data.username,
          company:res.data.company,
          jobTitle:res.data.jobTitle,
          typeIndex:typeIndex,
        })
      }
    })
  },
  updateUserInfo(){

  }
})