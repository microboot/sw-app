var api = require('../../../config/api.js')
var util = require('../../../utils/util.js')
var user = require('../../../utils/user.js')

var app = getApp()
Page({
  data: {
  },
  onLoad: function (options) {
    wx.clearStorageSync();
  },
  onReady: function () { },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  getPhoneNumber(e) {
    user.loginAndGetPhone(e.detail.code).then(res => {
      if (res.code == 200) {
        wx.navigateBack({delta:1})
      } else {
        wx.showToast({
          title: '登录授权失败',
        })
      }
    })
  },
  close(){
    wx.navigateBack({delta:1})
  }
})
