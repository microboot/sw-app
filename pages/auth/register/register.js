// pages/auth/register1/register1.js
var api = require("../../../config/api.js");
var util = require("../../../utils/util.js");

var app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    userId: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    let userId = options.userId;
    this.setData({
      userId
    });
  },

  // 头部回退按钮
  handlerGobackClick: function () {
    wx.navigateBack({
      delta: 1, // 返回上一级页面。
      success: function () { }
    });
  },
  // 头部首页按钮
  handlerGohomeClick: function () {
    wx.switchTab({
      url: "/pages/index/index"
    });
  },

  //点击去绑定手机号、修改手机号
  bindPhoneNumber: function (e) {
    if (e.detail.errMsg !== "getPhoneNumber:ok") {
      // 拒绝授权
      wx.navigateBack({
        delta: 2, // 返回上一级页面。                 
      })
      return;
    }
    console.log("app.globalData.hasLogin=", app.globalData.hasLogin);
    if (!app.globalData.hasLogin) {
      wx.showToast({
        title: "绑定失败：请先登录",
        icon: "none",
        duration: 2000
      });
      return;
    }
    util
      .request(
        api.AuthBindPhone,
        {
          iv: e.detail.iv,
          encryptedData: e.detail.encryptedData,
          userId: this.data.userId
        },
        "POST"
      )
      .then(function (res) {
        console.log("getPhoneNumber", res);
        if (res.errno === 0) {
          wx.showToast({
            title: "手机号绑定成功",
            icon: "success",
            duration: 2000
          });
          // wx.setStorageSync("userInfo", res.data.userInfo);
          wx.setStorageSync("token", res.data.token);
          wx.navigateTo({
            url: '/pages/auth/accountLogin/accountLogin',
          });
          // wx.navigateBack({
          //   delta: 1 // 返回上一级页面
          // });
        }
      });
  }
});
