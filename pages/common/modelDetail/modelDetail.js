const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
var WxParse = require('../../lib/wxParse/wxParse.js')
const app = getApp()

Page({
  data: {
    video: '', //视频
    setting: [], //表单内容
    canIUseGetUserProfile: false,
    show: false, //微信授权弹窗
    showModal: false,//授权手机号
    status: 0, //状态
    partUserName: '',
    partCompany: '',
    partJobTitle: '',
    activityId: '',
    id: '',
    title: '',
    address: ''
  },

  //右上角分享功能
  onShareAppMessage: function (options) {
    if (this.data.activityId) {
      //活动
      return {
        title: this.data.title,
        path: '/pages/modelDetail/modelDetail?type=1&id=' + this.data.activityId,
      }
    } else {
      //模块
      return {
        title: this.data.title,
        path: '/pages/modelDetail/modelDetail?type=3&id=' + this.data.id,
      }
    }
  },

  onLoad: function (options) {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
    if (options.content) {
      const content = decodeURIComponent(options.content)
      const title = options.title
      const id = options.id
      const status = options.status
      WxParse.wxParse('content', 'html', content, this)
      wx.setNavigationBarTitle({
        title: title  //修改title
      })
      this.setData({
        status: status,
        activityId: id,
        title: title
      })
      return
    }
    if (options.type == 1) {
      // type=1 活动
      const id = parseInt(options.id);
      this.activityInit(id)

    } else if (options.type == 2) {
      // type=2 资讯
      const result = JSON.parse(decodeURIComponent(options.data));
      if (result.content) {
        WxParse.wxParse('content', 'html', result.content, this)
      }
      wx.setNavigationBarTitle({
        title: result.subtitle  //修改title
      })
      this.setData({
        id: result.id,
        title: result.subtitle,
        video: result.video
      })
    } else if (options.type == 3) {
      // type=3 模块
      const id = parseInt(options.id);
      this.modelInit(id)
    }
  },

  modelInit (id) {
    wx.showLoading({
      title: '加载中...'
    });
    util.request(api.ModelDetail, { id: id }).then((res) => {
      if (res.errno == 0) {
        wx.hideLoading()
        const result = res.data
        WxParse.wxParse('content', 'html', result.content, this)
        wx.setNavigationBarTitle({
          title: result.subTitle  //修改title
        })
        this.setData({
          id: result.id,
          title: result.subTitle
        })
      } else {
        util.showErrorToast(res.errmsg);
      }
    });
  },


  activityInit (id) {
    wx.showLoading({
      title: '加载中...'
    });
    util.request(`${api.ActivityDetail}/${id}`).then((res) => {
      if (res.errno == 0) {
        wx.hideLoading()
        const result = res.data
        WxParse.wxParse('content', 'html', result.content, this)
        wx.setNavigationBarTitle({
          title: result.name  //修改title
        })
        this.setData({
          status: result.status,
          activityId: result.id,
          title: result.name,
          setting: result.setting
        })
      } else {
        util.showErrorToast(res.errmsg);
      }
    });
  },



  bindUsernameInput: function (e) {

    this.setData({
      partUserName: e.detail.value
    });
  },
  bindCompanyInput: function (e) {

    this.setData({
      partCompany: e.detail.value
    });
  },
  bindJobInput: function (e) {

    this.setData({
      partJobTitle: e.detail.value
    });
  },
  bindAddressInput: function (e) {

    this.setData({
      address: e.detail.value
    });
  },
  clearInput: function (e) {
    // console.log(11111, e.currentTarget)
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          partUserName: ''
        });
        break;
      case 'clear-company':
        this.setData({
          partCompany: ''
        });
        break;
      case 'clear-job':
        this.setData({
          partJobTitle: ''
        });
        break;
      case 'clear-address':
        this.setData({
          address: ''
        });
        break;
    }
  },

  accountLogin: function () {
    var that = this;

    if (!wx.getStorageSync('token')) {
      that.setData({
        show: true
      })
      return false;
    };


    // if (that.data.partUserName.length < 1 || that.data.partCompany.length < 1 || that.data.partJobTitle.length < 1) {
    if (that.data.partUserName.length < 1 || that.data.address.length < 1) {
      wx.showModal({
        title: '错误信息',
        content: '请输入完整信息',
        showCancel: false
      });
      return false;
    }

    util
      .request(
        api.ActivityJoin, {
        partUserName: that.data.partUserName,
        partCompany: that.data.partCompany,
        partJobTitle: that.data.partJobTitle,
        activityId: that.data.activityId,
        address: that.data.address
      },
        "POST"
      )
      .then(function (res) {
        if (res.errno === 0) {
          wx.showToast({
            title: "报名成功",
            icon: "success",
            duration: 2000,
          })
          that.setData({
            partUserName: '',
            address: '',
            partCompany: '',
            partJobTitle: ''
          });
        } else {
          util.showErrorToast(res.errmsg);
        }
      });
  },

  handleBuy: function () {
    if (this.data.activityId == 5) {
      wx.navigateToMiniProgram({
        appId: 'wxdb31ab24e8691f16',
        path: '',
        envVersion: 'release',// 打开正式版
        success (res) {
          // 打开成功
        },
        fail: function (err) {
          console.log(err);
        }
      })
      return
    }

  },

  getUserProfile () {
    user
      .checkLogin()
      .then(() => {
        //登录成功
        this.setData({
          show: false
        })
        app.globalData.hasLogin = true;
        console.log("登录成功！");
      })

      .catch(() => {
        // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
        // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
        wx.getUserProfile({
          desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
          success: (res) => {

            //判断是否登录//1。已经登录，去验证session是否过期，2.未登录，去登录
            user
              .loginByWeixin(res.userInfo)
              .then((res) => {
                // app.globalData.hasLogin = true;
                wx.setStorageSync("userInfo", res.data.userInfo);
                wx.setStorageSync("userId", res.data.userId);
                this.setData({
                  userId: res.data.userId,
                  show: false,
                  showModel: true
                })
              })
              .catch((err) => {
                app.globalData.hasLogin = false;
                util.showErrorToast("微信登录失败");
              });
          }
        })

      });
  },

  wxLogin: function (e) {
    console.log(e, e.detail.userInfo, "123-e");
    if (e.detail.userInfo == undefined) {
      app.globalData.hasLogin = false;
      util.showErrorToast("微信登录失败");
      return;
    }
    user
      .checkLogin()
      .then(() => {
        //登录成功
        this.setData({
          show: false
        })
        app.globalData.hasLogin = true;
        console.log("登录成功！");
      })
      .catch(() => {
        //判断是否登录//1。已经登录，去验证session是否过期，2.未登录，去登录
        user
          .loginByWeixin(e.detail.userInfo)
          .then((res) => {
            // app.globalData.hasLogin = true;
            wx.setStorageSync("userInfo", res.data.userInfo);
            wx.setStorageSync("userId", res.data.userId);
            this.setData({
              userId: res.data.userId,
              show: false,
              showModel: true
            })
          })
          .catch((err) => {
            app.globalData.hasLogin = false;
            util.showErrorToast("微信登录失败");
          });
      });
  },

  bindPhoneNumber: function (e) {
    let that = this
    if (e.detail.errMsg !== "getPhoneNumber:ok") {
      // 拒绝授权
      return;
    }
    console.log(app.globalData.hasLogin, "app.globalData.hasLogin");
    if (!that.data.userId) {
      wx.showToast({
        title: "绑定失败：请先登录",
        icon: "none",
        duration: 2000,
      });
      return;
    }
    util
      .request(
        api.AuthBindPhone,
        {
          iv: e.detail.iv,
          encryptedData: e.detail.encryptedData,
          userId: that.data.userId,
        },
        "POST"
      )
      .then(function (res) {
        console.log("getPhoneNumber", res);
        if (res.errno === 0) {
          that.setData({
            showModel: false,
            show: false
          })
          wx.setStorageSync("token", res.data.token);
        }
      });
  },


  onReady: function () { },
  onShow: function () { },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
});
