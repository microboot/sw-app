const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
   
  },
  onShow(){
    this.loadData();
  },
  loadData() {
    util.request(api.GetCoupons).then(res => {
      var couponMap = util.groupBy(res.data, "type");
      this.setData({
        couponMap: couponMap,
        typeKeys: Object.keys(couponMap)
      })
    })
  },
  ObtainCoupon(e) {
    var coupon = e.currentTarget.dataset.coupon;
    util.request(api.ObtainCoupon, { id: coupon.id }).then(res => {
      if(res.code==200){
        wx.showToast({
          title: '获取成功',
        })
        this.loadData();
      }else{
        wx.showToast({
          title: res.msg,
        })
      }
    })
  }
})