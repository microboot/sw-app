const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const drawQrcode = require("../../utils/weapp.qrcode.min.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    isHidden: true,// 默认隐藏
    coupons1:[],
    coupons2:[],
    coupons3:[]
  },
  showQr: function (e) {
    var coupon=e.currentTarget.dataset.coupon;
    this.setData({
      isHidden: false,
      currentCoupon:coupon
    });
    drawQrcode({
      width: 180,
      height: 180,
      x:0,
      y:0,
      canvasId: 'myQrcode',
      text: coupon.code
      })
  },
  close(){
    this.setData({
      isHidden: true
    });
    this.loadData();
  },
  switchTab: function(e) {
    const index = parseInt(e.currentTarget.dataset.index);
    this.setData({
      currentTab: index
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.loadData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  loadData(){
    util.request(api.GetMyCoupons).then(res=>{
      res.data.forEach(item=>{
        item.startTime=util.formatDate(new Date(item.startTime));
        item.endTime=util.formatDate(new Date(item.endTime));
      })
     this.setData({
       coupons1:res.data.filter(m=>m.enable==2||m.enable==1),
       coupons2:res.data.filter(m=>m.enable==3),
       coupons3:res.data.filter(m=>m.enable==4),
     }) 
    })
  }
})