const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/detailImg.png',
    videoUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/sp/hyh.mp4',
    changtuImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/changtu.png',
    mainTitle:'八宝祈福，文化妙会',
    time:'2024年2月10日-2月17日',
    add:'北京市石景山区首钢一高炉',
    tag:'郎园park',
    info:{},
    options:{}
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
  // wx.setNavigationBarTitle({ title:'京西有惊喜｜主题活动上新啦，快来看看吧'})
   util.request(`${api.ArticleInfo}/${options.id}`).then(res=>this.setData({info:res.data}));
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
   return util.share(getCurrentPages(),this.data.info.title);
    // return {
    //   title: this.data.info.title,
    //   path: '/pages/detail/detail?id=${options.oid}',
    //   imageUrl: this.data.info.picUrl,
    // }
  },
    //分享到朋友圈
    onShareTimeline: () => {
      return util.share(getCurrentPages(),this.data.info.title);
    },
})