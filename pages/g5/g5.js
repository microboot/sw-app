// pages/g5/g5.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  //分享到朋友圈
  onShareTimeline: () => {
    return {
      title: "吃好喝好｜京西春节餐厅指南",
      path: '/pages/g5/g5',
      imageUrl: "https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/swjpyq.png"
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '京西有惊喜｜吃好喝好，京西春节餐厅指南',
      path: '/pages/g5/g5',
      imageUrl: '/images/zhuanfa.png',
    }
  },
})