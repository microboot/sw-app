// pages/gonglue/gonglue.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
//分享到朋友圈
onShareTimeline: () => {
  return {
    title: "京西有惊喜｜打卡攻略指南",
    path: '/pages/gonglue/gonglue',
    imageUrl: "https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/pyq_gonglue.jpg"
  }
},

g1(){
  wx.navigateTo({
    url: '/pages/g1/g1',
  })
},
g2(){
  wx.navigateTo({
    url: '/pages/g2/g2',
  })
},
g3(){
  wx.navigateTo({
    url: '/pages/g3/g3',
  })
},
g4(){
  wx.navigateTo({
    url: '/pages/g4/g4',
  })
},
g5(){
  wx.navigateTo({
    url: '/pages/g5/g5',
  })
},
g6(){
  wx.navigateTo({
    url: '/pages/g6/g6',
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '京西有惊喜｜打卡攻略指南',
      path: '/pages/gonglue/gonglue',
      imageUrl: '/images/zhuanfa.png',
    }
  }
})