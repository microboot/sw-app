const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({
  data: {
    cardCur: 1,
    moreList: [],
    videoList: [],
    xcList: [],
    cwList: [],
    hsList: [],
    tyList: [],
    newsList: [],
    videoConfig:{},
    jingxiList:[],
    jingxiTop:[],
    bannerList:[]
  },
  
  onLoad() {
    app.getArticleList(0, 0,'news.info100').then(data => {
      data=data.sort((n1,n2)=>n1-n2);
      this.setData({ 
        newsList: data,
        topNewsList:data.filter(item=>item.isTop==1)
      })
    });
    app.getArticleList(0, 0,'news.video100').then(data => this.setData({ videoList: data }));
    app.getArticleList(1, 1).then(data => this.setData({ xcList: data }));
    app.getArticleList(2, 1).then(data => this.setData({ cwList: data }));
    app.getArticleList(10, 1).then(data => this.setData({ hsList: data }));
    app.getArticleList(11, 1).then(data => this.setData({ lnList: data }));
    app.getArticleList(12, 1).then(data => this.setData({ tyList: data }));
    app.getArticleList(13, 1).then(data => this.setData({ moreList: data }));
    var that=this;
    app.getConfigInfo("biz.video.close",config=>{
      that.setData({videoConfig:config});
      if(config.configValue=="0")
        that.getVideos();
    });
    app.getArticleList(0, 0,'jingxi').then(data => {
      data=data.sort((n1,n2)=>n1-n2);
      this.setData({ 
        jingxiList: data.slice(0,5),
        jingxiTop:data.filter(item=>item.isTop==1)[0]
      })
    });
    app.getArticleList(0, 0,'banner').then(data => {
      this.setData({bannerList: data })
    });
  },
  // onSwiperChange(e) {
  //   const query = wx.createSelectorQuery()
  //   const nodeRefs = query.selectAll('#indexvideo')
  //   nodeRefs.context(function (res) {
  //     console.log(res);
  //     res.forEach(item => {
  //       item.context.stop()
  //     })
  //   }).exec()
  // },

  bindfullscreenchange(e) {
    let fullScreen = e.detail.fullScreen //值true为进入全屏，false为退出全屏
    if (!fullScreen) { //退出全屏
      console.log('退出全屏')
      this.setData({
        fullScreen: false
      })

    } else { //进入全屏
      console.log('进入全屏')
      this.setData({
        fullScreen: true
      })
    }
  },

    //分享给好友
    onShareAppMessage: function (res) {
      return {
        title: '京西有惊喜',
        path: '/pages/index/index',
        imageUrl: '/images/zhuanfa.png',
      }
    },
    //分享到朋友圈
  onShareTimeline: () => {
    return {
      title: "京西有惊喜 京西消费节一站式资讯和服务平台",
      path: '/pages/index/index',
      imageUrl: "https://sjs.oss-cn-beijing.aliyuncs.com/swj2/swjpyq.jpg"
    }
  },

  gonews(){
    wx.switchTab({
      url: '/pages/news/news',
    })
  },
  goGonglue() {
    wx.switchTab({
      url: '/pages/gonglue/gonglue',
    })
  },
  goYouhui() {
    wx.navigateTo({
      url: '/pages/youhui/youhui',
    })
  },
  goActivity(){
    wx.switchTab({
      url: '/pages/activity/index',
    })
  },
  goQidai(){
    wx.navigateTo({
      url: '/pages/qidai/index',
    })
  },
  goZhenxuan(){
    wx.navigateTo({
      url: '/pages/zhenxuan/index',
    })
  },
  goCoupon(){
    wx.navigateTo({
      url: '/pages/coupon/index',
    })
  },
  goMap(){
    wx.navigateTo({
      url: '/pages/map/index',
    })
  },
  goMapList(){
    wx.switchTab({
      url: '/pages/map/list',
    })
  },
  goReview(){
    wx.navigateTo({
      url: '/pages/review/index',
    })
  },
  gollk() {
    wx.navigateTo({
      url: '/pages/hua/hua',
    })
  },
  goList() {
    wx.navigateTo({
      url: '/pages/list/list',
    })
  },
  goDetail() {
    wx.navigateTo({
      url: '/pages/detail/detail',
    })
  },
  DotStyle(e) {
    this.setData({
      DotStyle: e.detail.value
    })
  },
  // cardSwiper
  cardSwiper(e) {
    //解决滑动之后无法暂停上一个视频的问题
    this.setData({
      cardCur: e.detail.current
    })
    return;
    const query = wx.createSelectorQuery()
    const nodeRefs = query.selectAll('#indexvideo')
    nodeRefs.context(function (res) {
      console.log(res);
      res.forEach(item => {
        item.context.stop()
      })
    }).exec()

  },
  // towerSwiper触摸开始
  towerStart(e) {
    this.setData({
      towerStart: e.touches[0].pageX
    })
  },
  // towerSwiper计算方向
  towerMove(e) {
    this.setData({
      direction: e.touches[0].pageX - this.data.towerStart > 0 ? 'right' : 'left'
    })
  },
  // towerSwiper计算滚动
  towerEnd(e) {
    let direction = this.data.direction;
    let list = this.data.swiperList;
    if (direction == 'right') {
      let mLeft = list[0].mLeft;
      let zIndex = list[0].zIndex;
      for (let i = 1; i < list.length; i++) {
        list[i - 1].mLeft = list[i].mLeft
        list[i - 1].zIndex = list[i].zIndex
      }
      list[list.length - 1].mLeft = mLeft;
      list[list.length - 1].zIndex = zIndex;
      this.setData({
        swiperList: list
      })
    } else {
      let mLeft = list[list.length - 1].mLeft;
      let zIndex = list[list.length - 1].zIndex;
      for (let i = list.length - 1; i > 0; i--) {
        list[i].mLeft = list[i - 1].mLeft
        list[i].zIndex = list[i - 1].zIndex
      }
      list[0].mLeft = mLeft;
      list[0].zIndex = zIndex;
      this.setData({
        swiperList: list
      })
    }
  },
  getVideos() {
    util.request(api.DictData).then(res=>{
      var videos =res.data["bz_home_video"];
      this.setData({
        videoList: videos
      });
    });
  },
  bannerGo(e){
    var item=e.currentTarget.dataset.item;
    if(item.content){
      wx.navigateTo({
        url: `/pages/news/detail/detail?id=${item.id}`,
      })
    }
  }
})