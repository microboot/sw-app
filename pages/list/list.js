const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    options:{},
  },
  //分享到朋友圈
  onShareTimeline: () => {
    return util.share(getCurrentPages());
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
  wx.setNavigationBarTitle({ title:'京西有惊喜｜'+ options.title})
  app.getArticleList(options.typeId).then(data=>this.setData({list:data}));
  this.setData({
    options:options
  });
  util.getUrl(getCurrentPages());
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    //var title=decodeURIComponent(this.data.options.title);
    // var pages=getCurrentPages();
    // var url=util.getUrl(pages)
    // return {path:url}
    return util.share(getCurrentPages());
  },
})