const drawQrcode = require("../../../utils/weapp.qrcode.min.js");
const util = require("../../../utils/util.js");
const api = require("../../../config/api.js");
const user = require("../../../utils/user.js");
const { CardActivityLineList } = require("../../../config/api.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    drawQrcode({
      width: 200,
      height: 200,
      x:50,
      y:50,
      canvasId: 'myQrcode',
      text: '95fc2027-2f62-4827-8464-3df2a105a900'
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  scanQRCode: function() {
    wx.scanCode({
      success: function(res) {
        console.log(res.result);
        util.request(api.ConfirmCoupon,{code:res.result}).then(ret=>{

        })
      },
      fail: function(res) {
        console.log(res);
      }
    })
  }
})