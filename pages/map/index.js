const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const { CardActivityLineList } = require("../../config/api.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    LineList: [],
    isHidden: true,// 默认隐藏
    prize: []
  },
  toggleHidden: function () {
    this.setData({
      isHidden: !this.data.isHidden
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.checkLocationPermission();
   
  },
  onShow(){
    this.loadData();
  },
  loadData() {
    var activityId = 1;
    util.request(api.CardActivityLineList, { activityId: activityId }).then(res => {
      this.setData({ LineList: res.data });
    })
    util.request(api.GetMyPrize, { activityId: activityId }).then(res => {
      this.setData({ prize: res.data });
    })
  },
  doCard(e) {
    var node = e.currentTarget.dataset.node;
    this.getLocal(node, this.doPostCard)
  },
  doPostCard(node) {
    util.request(api.CardActivityRecoredSubmit, { lineId: node.id }).then((res => {
      if (res.code != 200) {
        wx.showToast({
          title: res.msg,
        })
        return;
      }
      if (res.data == 1) {
        this.setData({ isHidden: false })
      } else if (res.data == 0) {
        wx.showToast({
          title: '打卡成功',
        })
      }
      this.loadData();
    }))
  },
  checkLocationPermission() {
    let that = this;
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.userLocation']) {
          wx.showModal({
            title: '提示',
            content: '您还没有启用定位权限，是否前往设置开启？',
            success(res) {
              if (res.confirm) {
                wx.openSetting({
                  success(res) {
                    if (res.authSetting['scope.userLocation']) {
                      that.onLoad();
                    } else {
                      wx.authorize({ scope: 'scope.userLocation' })
                    }
                  }
                });
              } else if (res.cancel) {
                console.log(res.cancel);
                wx.authorize({ scope: 'scope.userLocation' })
              }
            }
          });
        }
      }
    });
  },
  getLocal(info, action) {
    var pos = info.name || "";
    var lat1 = parseFloat(info.lat) || 39.946138;
    var lon1 = parseFloat(info.lgt) || 116.186444;
    var radius = parseFloat(info.limitScope) || 0.5;//km
    var that = this;
    wx.getLocation({
      type: 'wgs84',
      success(res) {
        console.log(res);
        const latitude = res.latitude
        const longitude = res.longitude
        var r = util.getDistance(lat1, lon1, latitude, longitude);
        if (r > radius) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: `定位距离过远，请前往${pos}进行签到`,
          })
        } else {
          action(info);
        }
      }
    })
  },
})