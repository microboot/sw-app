// pages/map/list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  goMap(){
    wx.navigateTo({
      url: '/pages/map/index',
    })
  },
  goWeixun(){
    wx.navigateTo({
      url: '/pages/map/weixun',
    })
  },
  goManyou(){
    wx.navigateTo({
      url: '/pages/map/manyou',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },
     //分享到朋友圈
     onShareTimeline: () => {
      return {
        title: "京西打卡地图，吃喝玩乐、互动集章样样全，快来！",
        path: '/pages/map/list',
        imageUrl: "https://sjs.oss-cn-beijing.aliyuncs.com/swj2/swjpyq.jpg"
      }
    },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})