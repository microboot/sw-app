const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/tx.png',
    avatarUrl0:'https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/tx.png',
    isLogin:false,
    defaultUserInfo:{
      avatarUrl:"https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/tx.png",
      nickname:'未登录'
    },
    userInfo:{}
  },
  handleContact (e) {
    console.log(e.detail.path)
    console.log(e.detail.query)
  },
  goMyCoupon() {
    wx.navigateTo({
      url: '/pages/coupon/myCoupon',
    })
  },
  goCoupon(){
    wx.navigateTo({
      url: '/pages/coupon/index',
    })
  },
  goMessage(){
    wx.navigateTo({
      url: '/pages/message/index',
    })
  },
  goMyActivity(){
    wx.navigateTo({
      url: '/pages/myActivity/index',
    })
  },
  gosjs() {
    wx.navigateToMiniProgram({
     appId: 'wxcd0c65a3532df47c', // 需要跳转到指定的小程序appid
     path: 'pages/coinpark/coinpark',   // 打开的页面
     envVersion: 'release',
     success(res) {
      // 打开成功
      console.log(res);
     }
    })
   },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    var that=this;
    app.isLogin(status=>{
      that.setData({isLogin:status})
      if(status){
        var info=user.getLoginInfo();
        that.setData({
          userInfo:JSON.parse(info)
      })
      }else{
        that.setData({ userInfo:this.data.defaultUserInfo })        
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  onChooseAvatar(e) {
    const { avatarUrl } = e.detail 
    this.data.userInfo.avatarUrl=avatarUrl;
    this.setData({userInfo:this.data.userInfo})
    var that=this;
    wx.uploadFile({
      url: api.UploadFile, //仅为示例，非真实的接口地址
      filePath: avatarUrl,
      name: 'file',
      formData: {
        'user': 'test'
      },
      success (res){
        const data =JSON.parse(res.data);
        that.data.userInfo.avatarUrl=data.url;
        util.post(api.updateUser,that.data.userInfo)
      }
    })  
  },
  onNickName(e){
    const { value } = e.detail;
    if(!value)return;
    this.data.userInfo.nickName=value;
    util.post(api.updateUser,this.data.userInfo)
    this.setData({userInfo:this.data.userInfo})
  },
  outLogin(e){
    wx.clearStorageSync();
    this.setData({ userInfo:this.data.defaultUserInfo,
      isLogin:false })   
  },
  login(){
     
    wx.navigateTo({
      url: '/pages/auth/login/login',
    })
  },
  scanQRCode: function() {
    wx.scanCode({
      success: function(res) {
        console.log(res.result);
        util.request(api.ConfirmCoupon,{code:res.result}).then(ret=>{
          wx.showToast({
            title: ret.msg,
          })
        })
      },
      fail: function(res) {
        console.log(res);
      }
    })
  }
})