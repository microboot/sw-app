const util = require("../../utils/util.js");
const api = require("../../config/api.js");
const user = require("../../utils/user.js");
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardCur: 0,
    navbar: ['热点资讯', '视频专区'],  
    currentTab: 0,
    videoList: [],
    topNewsList:[],
    newsList: [],
  },
    //分享到朋友圈
    onShareTimeline: () => {
      return {
        title: "京西有惊喜 ｜ 活动资讯上新啦，点击查看更多内容",
        path: '/pages/news/news',
        imageUrl: "https://sjs.oss-cn-beijing.aliyuncs.com/sjs_swj/swjpyq.png"
      }
    },
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    app.getArticleList(0, 0,'news.info100').then(data => {
      data=data.sort((n1,n2)=>n1-n2);
      this.setData({ 
        newsList: data,
        topNewsList:data.filter(item=>item.isTop==1)
      })
    });
    var that=this;
    app.getConfigInfo("biz.video.close",config=>{
      that.setData({videoConfig:config});
      if(config.configValue=="0")
        app.getArticleList(0, 0,'news.video100').then(data =>
          {
            data.forEach(item=>item.isMp=item.resLink.indexOf(".mp")>0)
            that.setData({ videoList: data })
          }
           );
        else{
          that.setData({ navbar: ['热点资讯'] })
        }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '京西有惊喜 ｜ 活动资讯上新啦，点击查看更多内容',
      path: '/pages/news/news',
      imageUrl: '/images/zhuanfa.png',
    }
  },
    //跳转视频号对应的视频
    goVideo(event){
      var item=event.currentTarget.dataset.item;
      wx.openChannelsActivity({
        finderUserName: 'sph1CM3WgWyUDE3',
        feedId: item.resLink,
      })
    },
})