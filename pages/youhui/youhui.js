// pages/youhui/youhui.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    youhuiList:[ 
      {
        mainTitle:'AI尚首钢园',
        subTitle:'京西的潮流圣地',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui1.png', 
        appId: 'wxdb31ab24e8691f16',
        appPath:'/pages/index/index'
      },
      {
        mainTitle:'石景山游乐园',
        subTitle:'打卡童话世界',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui2.png',
        appId: 'wxc70bba599d2a0948',
        appPath:'/pages/index/index'
      },
      {
        mainTitle:'万达广场',
        subTitle:'大型城市商业综合体',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui3.png',
        appId: 'wx07dfb5d79541eca9',
        appPath:'/pages/index/index'
      },
      {
        mainTitle:'六工汇',
        subTitle:'首钢园北区核心区域',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui4.png',
        appId: 'wx8e6e4076bc4b6903',
      },
      {
        mainTitle:'京西大悦城',
        subTitle:'城市综合体项目',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui5.png',
        appId: 'wxb50c618ba98d3b3d',
        appPath:'/pages/activateDetailPage/activateDetailPage'
      },
      {
        mainTitle:'喜隆多',
        subTitle:'大型购物综合体',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui6.png',
        appId: 'wx128a2b5acbdc754a',
        appPath:'/pages/index/index'
      },
      {
        mainTitle:'驼铃古道 模式口',
        subTitle:'千年古道 百年老街',
        ImgUrl:'https://sjs.oss-cn-beijing.aliyuncs.com/swj2/youhui7.png',
        appId: 'wxd0d17c7b8248fe58',
        appPath:'/pages/index/index'
      },
  ],
  },
     //分享到朋友圈
     onShareTimeline: () => {
      return {
        title: "京西有惊喜，商圈消费购不停～",
        path: '/pages/youhui/youhui',
        imageUrl: "https://sjs.oss-cn-beijing.aliyuncs.com/swj2/swjpyq.jpg"
      }
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  goshougang(e){
    var appid=e.currentTarget.dataset.item.appId;
    var wxpath=e.currentTarget.dataset.item.appPath;
    wx.navigateToMiniProgram({
      appId: appid,
      path: wxpath,
      envVersion:"release",
      success(res){

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    return {
      title: '京西有惊喜｜惊喜福利',
      path: '/pages/youhui/youhui',
      imageUrl: '/images/zhuanfa.png',
    }
  },
})