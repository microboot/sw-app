// pages/youhui/youhui.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
   
  },
  goZhushan(){
    wx.navigateToMiniProgram({
      appId:'wx307cc11471683f24',
      path:'pages/index/index',
      envVersion:"release",
      success(res){

      }
    })
  },
     
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  goshougang(e){
    var appid=e.currentTarget.dataset.item.appId;
    var wxpath=e.currentTarget.dataset.item.appPath;
    wx.navigateToMiniProgram({
      appId: appid,
      path: wxpath,
      envVersion:"release",
      success(res){

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

})