function wechatPay(payParam, orderId) {
  wx.requestPayment({
    timeStamp: payParam.timeStamp,
    nonceStr: payParam.nonceStr,
    package: payParam.package,
    signType: payParam.signType,
    paySign: payParam.paySign,
    success: function (res) {
      console.log("=============支付过程成功=============");
      wx.redirectTo({
        url: "/pages/payResult/payResult?status=1&orderId=" + orderId
      });
    },
    fail: function (res) {
      console.log("=============支付过程失败=============");
      wx.redirectTo({
        url: "/pages/payResult/payResult?status=0&orderId=" + orderId
      });
    },
    complete: function (res) {
      console.log("=============支付过程结束=============");
    }
  })
}

module.exports = {
  wechatPay
}