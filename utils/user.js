/**
 * 用户相关服务
 */
const util = require("../utils/util.js");
const api = require("../config/api.js");

/**
 * Promise封装wx.checkSession
 */
function checkSession() {
  //：检验当前用户的session_key是否有效）
  return new Promise(function (resolve, reject) {
    wx.checkSession({
      success: function () {
        if (wx.getStorageSync("token")) {
          console.log("处于登录态");
          resolve(true);
        } else {
          reject(false);
        }
      },
      fail: function () {
        console.log("需要重新登录");
        reject(false);
      }
    });
  });
}

/**
 * Promise封装wx.login
 */
function login() {
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
}

/**
 * 调用微信登录
 */
function loginByWeixin(userInfo) {
  return new Promise(function (resolve, reject) {
    return login()
      .then(res => {
        console.log("用户的code：" + res.code)
        //登录远程服务器
        util
          .request(
            api.AuthLoginByWeixin,
            {
              code: res.code,
              userInfo: userInfo
            },
            "POST"
          )
          .then(res => {
            if (res.errno === 0) {
              //存储用户信息
              //   wx.setStorageSync("userInfo", res.data.userInfo);
              //   wx.setStorageSync("token", res.data.token);
              resolve(res);
            } else {
              reject(res);
            }
          })
          .catch(err => {
            reject(err);
          });
      })
      .catch(err => {
        reject(err);
      });
  });
}


// 获取用户openid
function getOpenID(params) {

}


/**
 * 判断用户是否登录
 */
function checkLogin() {
  //登录
  return new Promise(function (resolve, reject) {
    if (wx.getStorageSync("token")) {
      checkSession()
        .then(() => {
          // 检验当前用户的session_key有效）
          wx.navigateBack({
            delta: 1, // 返回上一级页面。
            success: function () {
              console.log("成功！");
            }
          });
          resolve(true);
        })
        .catch(() => {
          //无效
          reject(false);
        });
    } else {
      reject(false);
    }
  });
}

function loginAndGetPhone(phoneCode) {
  return new Promise(function (resolve, reject) {
    return login()
      .then(res => {
        console.log("用户的code：" + res.code)
        //登录远程服务器
        util
          .request(
            api.LoginAndBindPhone,
            {
              loginCode: res.code,
              phoneCode:phoneCode,
            },
            "POST"
          )
          .then(res => {
            if (res.code === 200) {
              //存储用户信息
              //   wx.setStorageSync("userInfo", res.data.userInfo);
              wx.setStorageSync("token", res.data.token);
              wx.setStorageSync("userInfo",JSON.stringify(res.data.userInfo));
              resolve(res);
            } else {
              reject(res);
            }
          })
          .catch(err => {
            reject(err);
          });
      })
      .catch(err => {
        reject(err);
      });
  });
}
function getLoginInfo(){
  return wx.getStorageSync("userInfo");
}
module.exports = {
  loginByWeixin,
  checkLogin,
  checkSession,
  loginAndGetPhone,
  getLoginInfo
};
