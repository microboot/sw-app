var api = require('../config/api.js');
var app = getApp();

function formatTime(date) {
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    var day = date.getDate()

    var hour = date.getHours()
    var minute = date.getMinutes()
    var second = date.getSeconds()


    return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
function formatDate(date) {
  
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  return [year, month, day].map(formatNumber).join('-');
}

function formatNumber(n) {
    n = n.toString()
    return n[1] ? n : '0' + n
}
/**
 * 封装微信的的request
 */
function request(url, data = {}, method = "GET") {
    return new Promise(function (resolve, reject) {
        wx.request({
            url: url,
            data: data,
            method: method,
            header: {
                'Content-Type': 'application/json',
                'token': wx.getStorageSync('token'),
                'wx_version': 'v2'
            },
            success: function (res) {
                if (res.statusCode == 200) {
                    if (res.data.code == 501) {
                        // 清除登录相关内容
                        try {
                            wx.removeStorageSync('userInfo');
                            wx.removeStorageSync('token');
                        } catch (e) {
                            // Do something when catch error
                            console.log(e)
                        }
                        // 切换到登录页面
                        wx.switchTab({
                            url: '/pages/auth/login/login'
                        });
                    } else {
                        resolve(res.data);
                        // console.log(res, "res---111");
                    }
                } else {
                    reject(res.errMsg);
                }

            },
            fail: function (err) {
                reject(err)
            }
        })
    });
}
function post(url, data)
{
    return this.request(url,data,"POST");
} 
function redirect(url) {
    //判断页面是否需要登录
    if (false) {
        wx.switchTab({
            url: '/pages/index/index'
        });
        return false;
    } else {
        wx.redirectTo({
            url: url
        });
    }
}

function showErrorToast(msg) {
    wx.showToast({
        title: msg,
        icon: "none",
        duration:3000
        // image: '/static/images/icon_error.png'
    })
}

function timeFormat(param) {
    return param < 10 ? '0' + param : param;
}
function groupBy(list, name,fun) {      
      var map={};
      list.forEach(m=>{
         if(fun)fun(m)
          var key=m[name].replaceAll(' ','');
          if(map[key]){
            map[key].push(m)
          }else{
            map[key]=[m];
          }       
      })
      return map;
    }

function setStorageSync(key,value,time){
var timestamp = Date.parse(new Date());// 当前时间
// 加上过期期限
var expiration = timestamp + time*1000*60; //缓存时间 单位分钟
// 存入缓存
wx.setStorageSync(key+'_time', time);//缓存时长
wx.setStorageSync(key+'_expiration', expiration);//过期时间
wx.setStorageSync(key, value);
}
function getStorageSync(key){  
  var timestamp = Date.parse(new Date());
  var data_expiration = wx.getStorageSync(key+'_expiration');//过期时间
  if (data_expiration) { 
     if (timestamp > data_expiration) {  
      wx.removeStorageSync(key)   
      wx.removeStorageSync(key+'_expiration')   
      wx.removeStorageSync(key+'_time')
      return null;   
     }
      }else if(wx.getStorageSync(key)){  
         wx.setStorageSync(key+'_expiration', timestamp)
    }
    return wx.getStorageSync(key);
}
function isEmpty(strIn) {
  if (strIn === undefined) {
      return true;
  } else if (strIn == null) {
      return true;
  } else if (strIn == "") {
      return true;
  } else if(!strIn){
      return true;
  }
  return false;
}
function getUrl(pages){
  // debugger;
  var currentPage = pages[pages.length - 1]; //获取当前页面的对象
  var url = currentPage.route ;//当前页面url
  var params=[];
  for(var key in currentPage.options){
    params.push(`${key}=${currentPage.options[key]}`)
  }
  if(params.length>0){
    url="/"+url+"?"+params.join('&');
  }
  console.log(url);
  return url;
}
function share(pages,title){
  var url=getUrl(pages);
  var ret={path:url};
  if(title)ret["title"]=title;
  return ret;
}
function getDistance(lat1, lon1, lat2, lon2) {
  var R = 6371;
  var dLat = (lat2 - lat1) * Math.PI / 180;
  var dLon = (lon2 - lon1) * Math.PI / 180;
  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var distance = R * c;
  return distance.toFixed(2);
}

module.exports = {
    formatTime,
    request,
    redirect,
    showErrorToast,
    timeFormat,
    post,
    groupBy,
    setStorageSync,
    getStorageSync,
    isEmpty,
    getUrl,
    share,
    getDistance,
    formatDate
}